DESTDIR?=
PREFIX?=/usr
BINDIR?=/bin
MANDIR?=/share/man
INSTALL?=install
CFLAGS?=-Wall -O2
OFLAGS?=-O1
CC?=gcc
OWLURL=https://gitlab.com/owl-lisp/owl/uploads/92375620fb4d570ee997bc47e2f6ddb7/ol-0.1.21.c.gz

bin/kal: kal.c
	-mkdir -p bin
	$(CC) $(CFLAGS) -o bin/kal kal.c
	make test

kal.c: kal.scm kal/parse.scm kal/main.scm bin/ol
	bin/ol $(OFLAGS) -o kal.c kal.scm

kal.fasl: kal.scm kal/parse.scm kal/main.scm
	$(OWL) --version || make get-owl
	$(OWL) -o kal.fasl kal.scm

test: bin/kal
	tests/check.sh bin/kal

install: bin/kal
	-mkdir -p $(DESTDIR)$(PREFIX)$(BINDIR)
	$(INSTALL) -m 755 bin/kal $(DESTDIR)$(PREFIX)$(BINDIR)/kal

uninstall:
	-rm -f $(DESTDIR)$(PREFIX)$(BINDIR)/kal

bin/ol:
	mkdir -p bin tmp
	test -f tmp/ol.c || curl $(OWLURL) | gzip -d > tmp/ol.c
	cc -O2 -o bin/ol tmp/ol.c

clean:
	-rm -f kal.c

mrproper:
	make clean
	-rm -rf owl-lisp-* bin tmp

.PHONY: test clean mrproper uninstall install get-owl
